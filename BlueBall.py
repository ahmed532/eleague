#!/usr/bin/env python
from __future__ import print_function
import numpy as np
import roslib
import sys
import rospy
import cv2
from std_msgs.msg import String
from std_msgs.msg import Float64
from sensor_msgs.msg import Image
from cv_bridge import CvBridge,CvBridgeError

diff = 0

class image_converter:
  global diff
  def __init__(self):
    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("csi_cam/image_raw",Image,self.callback)
    self.state = rospy.Publisher("state",Float64, queue_size=10)
    self.setpoint = rospy.Publisher("setpoint",Float64, queue_size=10)
    self.area_ratio = rospy.Publisher("area_ratio",Float64, queue_size=10)
    
  def callback(self,data):
    #print("callback")
    diff = 0.0
    radius = 0.0
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
      cv_image = cv2.resize(cv_image,(0,0), fx = 0.5, fy = 0.5) 
      width, height = cv_image.shape[:2]
      cv2.circle(cv_image,(height/2,width/2), 10, (0,0,255), -1)
    except CvBridgeError as e:
      pass
    hsv = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)
    lower_blue = np.array([55,90,90]) #Orange
    upper_blue = np.array([75,255,255])
    mask = cv2.inRange(hsv, lower_blue, upper_blue)
    res = cv2.bitwise_and(cv_image,cv_image, mask= mask)

    blur = cv2.GaussianBlur(mask,(9,9),0)
    edges = cv2.Canny(blur,100,300)
    circles = cv2.HoughCircles(blur,cv2.HOUGH_GRADIENT,1,100000000,
                            param1=350,param2=30,minRadius=10,maxRadius=9000)
    if circles != None:
     circles = np.uint16(np.around(circles))
     for i in circles[0,:]:
    # draw the outer circle
         cv2.circle(cv_image,(i[0],i[1]),i[2],(0,255,0),2)
    # draw the center of the circle
         radius = i[2]
         cv2.circle(cv_image,(i[0],i[1]),2,(0,0,255),3)
         diff = height/2.0 - i[0]
	 print(diff)
        
    cv2.imshow('cv_image',cv_image) 
    cv2.imshow('edges',edges)
    cv2.waitKey(3)
	
    try:
      self.state.publish(Float64(diff))
      self.setpoint.publish(Float64(0.0))
      self.area_ratio.publish(Float64(radius))
    except CvBridgeError as e:
      pass
      print(e)

def main(args):
  print("main")
  image_converter()
  rospy.init_node('image_converter', anonymous=True)
  try:
    rospy.spin()
  except KeyboardInterrupt:
    #print("Shutting down")
    pass
  cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)

