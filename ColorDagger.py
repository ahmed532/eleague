#!/usr/bin/env python
from __future__ import print_function
import numpy as np
import roslib
import sys
import rospy
import cv2
from std_msgs.msg import String
from std_msgs.msg import Float64
from sensor_msgs.msg import Image
from cv_bridge import CvBridge,CvBridgeError

diff = 0
HSV_image = "HSV image"
def nothing(x):    # dummy function so the createTrackbar() method can shut the F up.
    pass

class image_converter:
  global diff


  def segment_color(self,img, lower, upper):
      """
      :param img: Image to isolate teh color of
      :param lower: [lowerHue, lowerSat, lowerVal]
      :param upper: [upperHue, upperSat, upperVal
      :return: Isolated image
      """
      hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
      if lower[0] > upper[0]:
          # If the HSV values wrap around, then intelligently mask it

          upper1 = [179, upper[1], upper[2]]
          mask1 = cv2.inRange(hsv, np.array(lower), np.array(upper1))

          lower2 = [0, lower[1], lower[2]]
          mask2 = cv2.inRange(hsv, np.array(lower2), np.array(upper))

          mask = mask1 + mask2
	  return mask

      else:
          mask = cv2.inRange(hsv, np.array(lower), np.array(upper))
          return mask


    # return final

  def __init__(self):
    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("csi_cam/image_raw",Image,self.callback)
    self.state = rospy.Publisher("state",Float64, queue_size=10)
    self.setpoint = rospy.Publisher("setpoint",Float64, queue_size=10)
    self.area_ratio = rospy.Publisher("area_ratio",Float64, queue_size=10)

  def callback(self,data):
    #print("callback")
    diff = 0.0
    radius = 0.0
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
      cv_image = cv2.resize(cv_image,(0,0), fx = 0.5, fy = 0.5) 
      width, height = cv_image.shape[:2]
      #cv2.circle(cv_image,(height/2,width/2), 10, (0,0,255), -1)
    except CvBridgeError as e:
      pass
    # HSV Hue (0-360), Saturation (0-100), Value/Brightness (0-100) 
    cv2.namedWindow(HSV_image, cv2.WINDOW_NORMAL)
    cv2.createTrackbar('Low_H', HSV_image, 0, 179, nothing)
    cv2.createTrackbar('Low_S', HSV_image, 0, 255, nothing)
    cv2.createTrackbar('Low_V', HSV_image, 0, 255, nothing)
    cv2.createTrackbar('High_H', HSV_image, 0, 179, nothing)
    cv2.createTrackbar('High_S', HSV_image, 0, 255, nothing)
    cv2.createTrackbar('High_V', HSV_image, 0, 255, nothing)
    low_H = cv2.getTrackbarPos("Low_H", HSV_image)
    low_S = cv2.getTrackbarPos("Low_S", HSV_image)
    low_V = cv2.getTrackbarPos("Low_V", HSV_image)
    High_H = cv2.getTrackbarPos("High_H", HSV_image)
    High_S = cv2.getTrackbarPos("High_S", HSV_image)
    High_V = cv2.getTrackbarPos("High_V", HSV_image)
    lower_color = (low_H, low_S, low_V)
    upper_color = (High_H, High_S, High_V)
    mask = self.segment_color(cv_image, lower_color, upper_color)
    res = cv2.bitwise_and(cv_image,cv_image, mask= mask)
    blur = cv2.GaussianBlur(mask,(9,9),0)
    edges = cv2.Canny(blur,100,300)
    circles = cv2.HoughCircles(blur,cv2.HOUGH_GRADIENT,1,100000000,
                            param1=350,param2=30,minRadius=10,maxRadius=9000)
    if circles != None:
     circles = np.uint16(np.around(circles))
     for i in circles[0,:]:
    # draw the outer circle
         cv2.circle(cv_image,(i[0],i[1]),i[2],(0,255,0),2)
    # draw the center of the circle
         radius = i[2]
         cv2.circle(cv_image,(i[0],i[1]),2,(0,0,255),3)
         diff = height/2.0 - i[0]
	 print(diff)
        
    cv2.imshow('cv_image',cv_image) 
    cv2.imshow('RES',res)
    cv2.imshow('Edges',mask)
    cv2.imshow('edges',edges)
    cv2.waitKey(3)
	
    try:
      self.state.publish(Float64(diff))
      self.setpoint.publish(Float64(0.0))
      self.area_ratio.publish(Float64(radius))
    except CvBridgeError as e:
      pass
      print(e)


def main(args):
  print("main")
  image_converter()
  rospy.init_node('image_converter', anonymous=True)
  try:
    rospy.spin()
  except KeyboardInterrupt:
    #print("Shutting down")
    pass
    cv2.destroyAllWindows()

if __name__ == '__main__':
    pass
    main(sys.argv)

