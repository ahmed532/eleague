import rospy
from std_msgs.msg import Float64
from std_msgs.msg import String, Int32
import numpy as np
from time import sleep
import time
import serial

import numpy as np
import roslib
import sys
import rospy
import cv2
from std_msgs.msg import String
from std_msgs.msg import Float64
from sensor_msgs.msg import Image
from cv_bridge import CvBridge,CvBridgeError

cirCount = 0
leftCount = 0
rightCount = 0


ser = serial.Serial('/dev/ttyS0', 19200, timeout = 1)


JetsonColorInfo = {'yellow': {'lower': [23, 133, 117],
                              'upper': [39, 255, 255],
                              'x-x': 30,
                              'area': 0.0,
                              'diff': 0.0,
                              'name': 'yellow'},
                   'blue':   {'lower': [80, 123, 124],
                              'upper': [103, 255, 255],
                              'x-x': 30,
                              'area': 0.0,
                              'diff': 0.0,
                              'name': 'blue'},
                   'purple': {'lower': [109, 70, 131],
                              'upper': [142, 212, 255],
                              'x-x': 100,
                              'area': 0.0,
                              'diff': 0.0,
                              'name': 'purple'},
                   'red':    {'lower': [0, 25, 24],
                              'upper': [11, 255, 255],
                              'x-x': 100,
                              'area': 0.0,
                              'diff': 0.0,
                              'name': 'red'}}


Pi1ColorInfo = {'yellow': {'lower': [23, 133, 117],
                              'upper': [39, 255, 255],
                              'x-x': 30,
                              'area': 0.0,
                              'diff': 0.0,
                              'name': 'yellow'},
                   'blue':   {'lower': [80, 123, 124],
                              'upper': [103, 255, 255],
                              'x-x': 30,
                              'area': 0.0,
                              'diff': 0.0,
                              'name': 'blue'},
                   'purple': {'lower': [109, 70, 131],
                              'upper': [142, 212, 255],
                              'x-x': 100,
                              'area': 0.0,
                              'diff': 0.0,
                              'name': 'purple'},
                   'red':    {'lower': [0, 25, 24],
                              'upper': [11, 255, 255],
                              'x-x': 100,
                              'area': 0.0,
                              'diff': 0.0,
                              'name': 'red'}}


Pi2ColorInfo = {'yellow': {'lower': [23, 133, 117],
                              'upper': [39, 255, 255],
                              'x-x': 30,
                              'area': 0.0,
                              'diff': 0.0,
                              'name': 'yellow'},
                   'blue':   {'lower': [80, 123, 124],
                              'upper': [103, 255, 255],
                              'x-x': 30,
                              'area': 0.0,
                              'diff': 0.0,
                              'name': 'blue'},
                   'purple': {'lower': [109, 70, 131],
                              'upper': [142, 212, 255],
                              'x-x': 100,
                              'area': 0.0,
                              'diff': 0.0,
                              'name': 'purple'},
                   'red':    {'lower': [0, 25, 24],
                              'upper': [11, 255, 255],
                              'x-x': 100,
                              'area': 0.0,
                              'diff': 0.0,
                              'name': 'red'}}



class Dagu:
    def __init__(self, i, other_color, goal_color, catch_ball_size, CI):
        rospy.init_node('dagu_%d'% (i,) , anonymous=True)
        rospy.Subscriber('/master_dagu/action', String, self.action_callback)
        rospy.Subscriber('control_effort', Float64, self.effort_callback)
        
        self.done = rospy.Publisher('done', String, queue_size=10)
        self.gribber = rospy.Publisher('toggle_led', Int32, queue_size=10)
        self.last_action = rospy.Publisher('last_action', String, queue_size=10)
        self.rate = rospy.Rate(10) # 10hz
        self.dagu_fun = {"move_to_ball": self.move_to_ball, "stop": self.stop,\
                         "make_pass": self.make_pass, "seek_ball": self.seek_ball,\
                         "catch_ball": self.catch_ball, "release_ball": self.release_ball,\
                         "face": self.face,\
        }
        self.id = i
        self.effort = 0
        self.is_done = 'no'
        self.m_last_action = 'fuck'
        self.diff = 0
        self.last_diff = 0
        self.last_ball_time = 0
        self.min_duration = 0.5
        self.has_ball = False
        self.last_action_i = -1
        self.catch_ball_size = catch_ball_size

        #Vision section
        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("csi_cam/image_raw",Image,self.vision_callback)
        self.ColorInfo = CI
        self.pid_color = 'null'
        self.goal_color = goal_color
        self.other_color = other_color


        #PID section
        self.state = rospy.Publisher("state",Float64, queue_size=10)
        self.setpoint = rospy.Publisher("setpoint",Float64, queue_size=10)
        
        #Start with gribber open
        sleep(1)
        self.release_ball()
        
    def action_callback(self, data):
        l = data.data.split()
        action_i, i, a, pid = int(l[0]), int(l[1]), l[2], l[3]
        if action_i <= self.last_action_i:
            return
        self.last_action_i = action_i
        
        try:
            self.pid_color = pid
            self.setpoint.publish(Float64(0.0))
            if i == self.id:
                self.is_done = 'no'
                print('running', a)
                self.m_last_action = a
                #x = raw_input('ok?')
                self.dagu_fun[a]()
                self.is_done = 'yes'
                #self.rate.sleep()
                print('done!')
                #x = raw_input('done!')
            else:
                #print('not for me')
                self.stop()
        except KeyboardInterrupt:
            self.stop()
            quit()

    def update_last_seen(self, color):
        if color['diff'] != 0:
            self.last_diff = color['diff']
            self.last_ball_time = time.time()
                
    def sensors(self):
        while not rospy.is_shutdown():
            #print('publishing', self.is_done)
            self.done.publish(String(self.is_done))
            self.last_action.publish(String(self.m_last_action))
            print(self.ColorInfo['blue'])
            print(self.ColorInfo['red'])
            print('='*50)
            #print(self.radius, self.is_done)
            #self.ball_ratio.publish(100)
            self.rate.sleep()

    def stop(self):
        command_right = chr(0xC2)
    	command_left = chr(0xCA)
    	ser.write(command_right)
    	ser.write(chr(0))
    	ser.write(command_left)
    	ser.write(chr(0))

    def rlc(self, speed):
        command_right = chr(0xC1)
        command_left = chr(0xC9)
        ser.write(command_right)
        ser.write(chr(speed))
        ser.write(command_left)
        ser.write(chr(speed))

    def rrc(self, speed):
        command_right = chr(0xC2)
        command_left = chr(0xCA)
        ser.write(command_right)
        ser.write(chr(speed))
        ser.write(command_left)
        ser.write(chr(speed)) 
        

    def face(self):
        while self.ColorInfo[self.pid_color] == 0.0:
            self.rrc(45)
        self.stop()
        return
        
    def seek_ball(self):
        while self.ColorInfo['red']['area'] < self.catch_ball_size:
            if self.ColorInfo['red']['area'] == 0.0 and time.time() - self.last_ball_time > self.min_duration:
                if self.last_diff < 0:
                    self.rrc(45)
                    #print('left')
                else:
                    self.rlc(45)
                    #print('right')
            else:
                #print('ball')
                self.move_to_ball(70)
        sleep(0.1)
        self.catch_ball()
        self.stop()
        self.has_ball = True

    def make_pass(self):
        if not self.has_ball:
            self.seek_ball()
            # pass itself
            print('kicking')
            sleep(0.3)
            self.catch_ball()
        self.move_to_ball(120)
        sleep(0.7)
        self.release_ball()
        sleep(0.1)
        self.move_backward(100)
        sleep(0.1)
        self.stop()



    def move_backward(self, speed):
        command_right = chr(0xC2)
        command_left = chr(0xC9)
        ser.write(command_right)
        ser.write(chr(speed))
        ser.write(command_left)
        ser.write(chr(speed))

    def move_to_ball(self, base_speed=50):
        right_speed = 0
        left_speed = 0
        if self.effort >= 0:
	    left_speed += int(self.effort)
        else:
	    right_speed += int(-self.effort)
        #print("moving", left_speed, right_speed)
        command_right = chr(0xC1)
        command_left = chr(0xCA)
        if right_speed > 120:
            right_speed = 120
        if left_speed > 120:
            left_speed = 120
        ser.write(command_right)
        ser.write(chr(base_speed + right_speed))
        ser.write(command_left)
        ser.write(chr(base_speed + left_speed))


    def catch_ball(self):
        print("catch ball")
        self.gribber.publish(Int32(0))
        self.has_ball = True

    def release_ball(self):
        print("release ball")
        self.gribber.publish(Int32(1))
        self.has_ball = False
    
    def effort_callback(self, data):
        self.effort = data.data


    #vision section

    def vision_callback(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
            cv_image = cv2.resize(cv_image,(0,0), fx = 0.5, fy = 0.5)
            if self.pid_color != 'null':
                if self.pid_color == 'red':
                    self.circle_callback(cv_image, self.ColorInfo['red'])
                    self.update_last_seen(self.ColorInfo['red'])
                else:
                    self.rect_callback(cv_image, self.ColorInfo[self.pid_color])
                self.state.publish(Float64(self.ColorInfo[self.pid_color]['diff']))
        except CvBridgeError as e:
            pass
        return

    def rect_callback(self, cv_image, color):
        global cirCount
        global leftCount
        global rightCount
        width, height = cv_image.shape[:2]
        hsv = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)
        lower_blue = np.array(color['lower'])#64, 150, 120
        upper_blue = np.array(color['upper'])
        mask = cv2.inRange(hsv, lower_blue, upper_blue)
        res = cv2.bitwise_and(cv_image,cv_image, mask= mask)
        blur = cv2.GaussianBlur(mask,(21,21),0)
        blur2 = cv2.GaussianBlur(mask,(5,5),0)
        #interest = cv2.cvtColor(blur2, cv2.COLOR_BGR2GRAY)
        blur3 = cv2.GaussianBlur(mask,(11,11),0)
        blur4 = cv2.GaussianBlur(mask,(13,13),0)
        edges = cv2.Canny(blur4,200,300)
        output = cv2.connectedComponentsWithStats(blur)
        cv2.circle(cv_image,(height/2,width/2), 10, (0,0,255), -1)
        # Get the results
        # The first cell is the number of labels
        num_labels = output[0]
        # The second cell is the label matrix
        labels = output[1]
        # The third cell is the stat matrix
        stats = output[2]
        # The fourth cell is the centroid matrix
        centroids = output[3]
        count = 0
        diff = 0.0
        area = 0.0
        crop_img = blur2
        resR = 0
        resL = 0
        l = [x[4] for x in stats]
        idx = np.argmax(l)
        l[idx] = -1000000 
        idx = np.argmax(l)
        x = stats[idx]
        if x[4] > 1500 and abs(x[2]-x[3]) < color['x-x'] and count < 1:
            count += 1
            cv2.circle(cv_image,(int(centroids[idx][0]),int(centroids[idx][1])), 1, (0,0,255), -1)
            diff = height/2 - centroids[idx][0]
            area = x[4]/float(width*height)
            #print("Area, diff", area, diff)
            cv2.rectangle(cv_image,(x[0],x[1]),(x[0]+x[2],x[1]+x[3]),(0,255,0),3)      
        #cv2.imshow('Edges',mask)
        #cv2.imshow("Test",cv_image)
        #cv2.waitKey(3)
        color['area'] = area
        color['diff'] = diff



    def circle_callback(self, cv_image, color):
        diff = 0.0
        radius = 0.0
        width, height = cv_image.shape[:2]
        hsv = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)
        lower_blue = np.array(color['lower']) #Orange
        upper_blue = np.array(color['upper'])
        mask = cv2.inRange(hsv, lower_blue, upper_blue)
    
        #lower_blue2 = np.array([170,140,0]) #Orange
        #upper_blue2 = np.array([179,255,255])
        #mask2 = cv2.inRange(hsv, lower_blue2, upper_blue2)
    
        #mask3 = cv2.bitwise_or(mask,mask2)

        res = cv2.bitwise_and(cv_image,cv_image, mask= mask)

        blur = cv2.GaussianBlur(mask,(9,9),0)
        edges = cv2.Canny(blur,100,300)
        circles = cv2.HoughCircles(blur,cv2.HOUGH_GRADIENT,1,10000,
                            param1=300,param2=28,minRadius=1,maxRadius=9000)
        if circles != None:
            circles = np.uint16(np.around(circles))
            for i in circles[0,:]:
                # draw the outer circle
                cv2.circle(cv_image,(i[0],i[1]),i[2],(0,255,0),2)
                # draw the center of the circle
                radius = i[2]
                #print(radius)
                cv2.circle(cv_image,(i[0],i[1]),2,(0,0,255),3)
                diff = height/2.0 - i[0]
	        #print(diff)
        #cv2.circle(cv_image,(height/2,width/2), 10, (0,0,255), -1)
        #cv2.imshow('cv_image',cv_image) 
        #cv2.imshow('edges',edges)
        #cv2.imshow('edges2',mask)
        #cv2.waitKey(3)
        color['area'] = radius
        color['diff'] = diff
	





        
if __name__ == '__main__':
    try:
        d = Dagu(0, '5ara', 'yellow', 40, JetsonColorInfo)
        d.sensors()
    except rospy.ROSInterruptException:
        pass
