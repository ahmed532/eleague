import rospy
from std_msgs.msg import Float64
from std_msgs.msg import String
import numpy as np
from time import sleep
import serial
#ser = serial.Serial('/dev/ttyS0', 19200, timeout = 1)
class Dagu:
    def __init__(self, i):
        rospy.init_node('dagu_%d'% (i,) , anonymous=True)
        rospy.Subscriber('/master_dagu/action', String, self.action_callback)
        rospy.Subscriber('control_effort', Float64, self.effort_callback)
        rospy.Subscriber('area_ratio', Float64, self.area_ratio_callback)
        self.ball_ratio = rospy.Publisher('ball_ratio', Float64, queue_size=10)
        self.rate = rospy.Rate(10) # 10hz
        self.dagu_fun = {"move_to_ball": self.move_to_ball, "stop": self.stop}
        self.id = i
        self.radius = 100
        self.effort = 0
        
    def action_callback(self, data):
        i, a = int(data.data.split()[0]), data.data.split()[1]
        if i == self.id:
            self.dagu_fun[a]()

    def area_ratio_callback(self, data):
        print(data.data)
        self.radius = 100
    
    def sensors(self):
        while not rospy.is_shutdown():
            #self.ball_ratio.publish(np.random.randint(100, size=1)[0])
            print(self.radius)
            self.ball_ratio.publish(100)
            self.rate.sleep()

    def stop(self):
        command_right = chr(0xC2)
    	command_left = chr(0xCA)
    	ser.write(command_right)
    	ser.write(chr(0))
    	ser.write(command_left)
    	ser.write(chr(0))

    def move_to_ball(self):
        base_speed = 97
        right_speed = 0
        left_speed = 0
        if self.effort >= 0:
	    left_speed += int(self.effort)
        else:
	    right_speed += int(-self.effort)
        print("moving", left_speed, right_speed)
        command_right = chr(0xC1)
        command_left = chr(0xCA)
        if right_speed > 120:
            right_speed = 120
        if left_speed > 120:
            left_speed = 120
        #ser.write(command_right)
        #ser.write(chr(base_speed + right_speed))
        #ser.write(command_left)
        #ser.write(chr(base_speed + left_speed))

    def effort_callback(self, data):
        self.effort = data.data
        
if __name__ == '__main__':
    try:
        d = Dagu(1)
        d.sensors()
    except rospy.ROSInterruptException:
        pass
