#!/usr/bin/env python
from __future__ import print_function
import numpy as np
import roslib
import sys
import rospy
import cv2
from std_msgs.msg import String
from std_msgs.msg import Float64
from sensor_msgs.msg import Image
from cv_bridge import CvBridge,CvBridgeError

cirCount = 0
leftCount = 0
rightCount = 0

class image_converter:


  def __init__(self):

    self.area_ratio = rospy.Publisher("yellow_area",Float64, queue_size=10)


    #self.arrow_direction = rospy.Publisher("arrow_direction",String, queue_size=10)
    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("/dagu_0/csi_cam/image_raw",Image,self.callback)

  def callback(self,data):
    global cirCount
    global leftCount
    global rightCount
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
      #print "got img"
      cv_image = cv2.resize(cv_image,(0,0), fx = 0.5, fy = 0.5) 
      width, height = cv_image.shape[:2]
    except CvBridgeError as e:
      pass
     # print(e)
    hsv = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)
    lower_blue = np.array([4, 117, 98])#64, 150, 120
    upper_blue = np.array([15, 255,255])
    mask = cv2.inRange(hsv, lower_blue, upper_blue)
    res = cv2.bitwise_and(cv_image,cv_image, mask= mask)
    blur = cv2.GaussianBlur(mask,(21,21),0)
    blur2 = cv2.GaussianBlur(mask,(5,5),0)
    #interest = cv2.cvtColor(blur2, cv2.COLOR_BGR2GRAY)
    blur3 = cv2.GaussianBlur(mask,(11,11),0)
    blur4 = cv2.GaussianBlur(mask,(13,13),0)
    edges = cv2.Canny(blur4,200,300)
    output = cv2.connectedComponentsWithStats(blur)
    cv2.circle(cv_image,(height/2,width/2), 10, (0,0,255), -1)
# Get the results
# The first cell is the number of labels
    num_labels = output[0]
# The second cell is the label matrix
    labels = output[1]
# The third cell is the stat matrix
    stats = output[2]
# The fourth cell is the centroid matrix
    centroids = output[3]
    count = 0
    diff = 0.0
    area = 0.0
    crop_img = blur2
    resR = 0
    resL = 0
    l = [x[4] for x in stats]
    idx = np.argmax(l)
    l[idx] = -1000000 
    idx = np.argmax(l)
    x = stats[idx]
    #yy = sorted(output, key=lambda s: s[2][4], reverse=True)
    # The third cell is the stat matrix
    
    #for idx,x in enumerate(stats):
      #cv2.rectangle(cv_image,(stats[x][0],stats[x][1]),(stats[x][0]+stats[x][2],stats[x][1]+stats[x][3]),(0,255,0),3)
    if x[4] > 1500 and abs(x[2]-x[3]) < 30 and count < 1:
      #print("index is", idx)
      #print("got rectangle")
      #self.is_box.publish(String('yes'))
      count += 1
      cv2.circle(cv_image,(int(centroids[idx][0]),int(centroids[idx][1])), 1, (0,0,255), -1)
      diff = height/2 - centroids[idx][0]
      area = x[4]/float(width*height)
      print("Area", area)
      print("diff", diff)
      cv2.rectangle(cv_image,(x[0],x[1]),(x[0]+x[2],x[1]+x[3]),(0,255,0),3)      
		

    cv2.imshow('Edges',mask)
    cv2.imshow("Test",cv_image)
    cv2.waitKey(3)
	
    try:
      self.area_ratio.publish(Float64(area))
    except CvBridgeError as e:
      pass
    #  print(e)

def main(args):
  image_converter()
  rospy.init_node('image_converter', anonymous=True)
  try:
    rospy.spin()
  except KeyboardInterrupt:
    #print("Shutting down")
    pass
  cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
