#!/bin/env python
import serial
import time
import rospy
from std_msgs.msg import String
from std_msgs.msg import Float64
from subprocess import call
import time
import datetime
import numpy as np
import std_msgs.msg

class State:
    def __init__(self):
        self.dagus = [{}, {}, {}]
        self.dagus[0]['ball_ratio'] = 0.0
        self.dagus[1]['ball_ratio'] = 0.0
        self.dagus[2]['ball_ratio'] = 0.0
        self.dagus[0]['done'] = 'no'
        self.dagus[1]['done'] = 'no'
        self.dagus[2]['done'] = 'no'
        self.dagus[0]['last_action'] = 'fuck'
        self.dagus[1]['last_action'] = 'fuck'
        self.dagus[2]['last_action'] = 'fuck'
        for i, d in enumerate(self.dagus):
            rospy.Subscriber('/dagu_%d/ball_ratio' % (i,), Float64, self.make_callback(i, 'ball_ratio'))
            rospy.Subscriber('/dagu_%d/done' % (i,), String, self.make_callback(i, 'done'))
            rospy.Subscriber('/dagu_%d/last_action' % (i,), String, self.make_callback(i, 'last_action'))

    def make_callback(self, i, x):
        def callback(data):
            self.dagus[i][x] = data.data
        return callback


colors = {0: {'color': 'blue',
              'other': 'purple'},
          1: {'color': 'purple',
              'other': 'blue'},
          'goal': 'yellow'}

class CatchBall:
    def __init__(self, p):
        self.passer = p
        self.actions = [(self.passer, "seek_ball", 'red'),
                        (self.passer, "catch_ball", 'red'),
                        (self.passer, "stop", 'null')]
        

class CloserCatchBall:
    def __init__(self, p, r):
        self.p = p
        self.r = r
        self.actions = [(self.p, "seek_ball", 'red'),
                        (self.p, "catch_ball", 'red'),
                        (self.p, "stop", 'null')]
        
        
class PassShot:
    def __init__(self, p, r):
        self.p = p
        self.r = r
        self.actions = [(self.p, "stop", 'null'),
                        (self.p, "seek_ball", 'red'),
                        (self.p, "catch_ball", 'red'),
                        (self.p, "stop", 'null'),
                        (self.p, "face", colors[self.r]['color']),
                        (self.p, "make_pass", 'red'),
                        (self.p, "stop", 'null'),
                        (self.r, "seek_ball", 'red'),
                        (self.r, "catch_ball", 'red'),
                        (self.r, "face", colors['goal']),
                        (self.r, "make_pass", colors['goal']),
                        (self.r, "stop", 'null')]
                        

class ThrowGoal:
    def __init__(self, p, r, g):
        self.p = p
        self.r = r
        self.g = g
        self.actions = [(g, "keep", 'red'),
                        (p, "seek_ball", 'red'),
                        (p, "catch_ball", 'red'),
                        (p, 'face_player', colors[self.r]),
                        (p, 'make_pass', 'red'),
                        (r, 'seek_ball', 'red'),
                        (r, 'catch_ball', 'red'),
                        (p, 'move_to_goal', colors['goal']),
                        (p, 'face', colors[self.r]),
                        (r, 'face', colors[self.p]),
                        (r, 'make_pass', colors[self.p]),
                        (p, 'seek_ball', 'red'),
                        (p, 'catch_ball', 'red'),
                        (p, 'face', colors['goal']),
                        (p, 'make_pass', colors['goal']),
                        (p, 'stop', 'null')]
                         
    
class MasterDagu:
    def __init__(self):
        self.state = State()
        rospy.init_node('master_x', anonymous=True)
        self.send_action = rospy.Publisher('/master_dagu/action', String, queue_size=10)
        self.rate = rospy.Rate(5)
        self.last_action_i = 0
    
    def run(self):
        while True:
            print(self.state.dagus)
            i = self.near_ball()
            if self.state.dagus[i]['ball_ratio'] != 0:
                self.send_action.publish(String(str(i) + " make_pass"))
            #time.sleep(5)
            #self.send_action.publish(String("1 stop"))
            #self.send_action.publish(String("0 stop"))
            #raw_input()
            self.rate.sleep()
        rospy.spin()

    def near_ball(self):
        l = [d['ball_ratio'] for d in self.state.dagus]
        print(np.argmax(l))
        return np.argmax(l)

    def exec_tactic(self, t):
        f = 0
        for p in t.actions:
            i = p[0]
            a = p[1]
            d = p[2]
            print(i, a)
            ll = String(" ".join((str(self.last_action_i), str(i), a, d)))
            self.send_action.publish(ll)
            self.rate.sleep()
            self.last_action_i += 1
            while self.state.dagus[i]['last_action'] != a:
                self.send_action.publish(ll)
                self.rate.sleep()
            while self.state.dagus[i]['done'] != 'yes':
                #print('waiting for', i, a)
                #self.send_action.publish(String(str(i) + ' ' + a))
                self.rate.sleep()
            self.rate.sleep()
            f = f + 1
            
        

d = MasterDagu()
d.exec_tactic(CatchBall(0))
