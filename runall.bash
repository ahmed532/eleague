#!/bin/bash
screen -S pisc -p 1 -X stuff $'roslaunch jetson_csi_cam jetson_csi_cam.launch width:=640 height:=360 fps:=30\r'
screen -S pisc -p 2 -X stuff $'python nepton.py\r'
screen -S pisc -p 3 -X stuff $'rosrun pid controller _upper_limit:=20 _lower_limit:=-20 _Kp:=0.3 _Ki:=0 _Kd:=0\r'
screen -S pisc -p 4 -X stuff $'python PurpleRect.py\r'
screen -S pisc -p 5 -X stuff $'rosrun rosserial_python serial_node.py _port:=/dev/ttyACM0 _baud:=115200\r'
screen -S pisc -p 6 -X stuff $'python dagu0.py\r'

#you have to run$ python master_dagu.py
