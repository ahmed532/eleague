#!/usr/bin/pyhton
import serial
import rospy
from std_msgs.msg import Int32
rospy.init_node('ultra_node_jetson', anonymous=True)

pub = rospy.Publisher('ultraJetson', Int32, queue_size=10)
rate = rospy.Rate(5)
ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
d = ''
while True:
    d = ''
    c = ''
    while c != '\r':
       d = d + c
       c = ser.read()
    ser.read()
    print(d)
    pub.publish(Int32(int(d)))
    #rate.sleep()

print('done')

